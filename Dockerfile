FROM openjdk:8-jre-alpine
COPY build/libs .
CMD ["java" ,"-Xms600m" ,"-Xmx600m", "-jar", "challenge-0.0.1-SNAPSHOT.jar"]