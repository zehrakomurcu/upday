package com.upday.challenge.repository

import com.upday.challenge.model.Author
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@DataJpaTest
class AuthorRepositoryTest {
    @Autowired
    lateinit var repository: AuthorRepository

    @Test
    fun shouldSave() {
        // given
        val id = UUID.randomUUID()
        val author = Author()
        author.id = id
        author.firstName = "First name"
        author.lastName = "Last name"
        val authorEntity = repository.save(author)


        // then
        Assertions.assertThat(authorEntity.id).isNotNull()
        Assertions.assertThat(authorEntity.firstName).isEqualTo("First name")
        Assertions.assertThat(authorEntity.lastName).isEqualTo("Last name")

    }

    @Test
    fun shouldReturnByName() {
        // given
        val id = UUID.randomUUID()
        val author = Author()
        author.id = id
        author.firstName = "First name"
        author.lastName = "Last name"
        repository.save(author)

        //when
        val authorEntity = repository.findByName(firstName = "First name", lastName = "Last name")

        // then
        Assertions.assertThat(authorEntity?.id).isNotNull()
        Assertions.assertThat(authorEntity?.firstName).isEqualTo("First name")
        Assertions.assertThat(authorEntity?.lastName).isEqualTo("Last name")

    }

    @Test
    fun shouldReturnNullWhenNotExists() {
        // given
        val id = UUID.randomUUID()
        val author = Author()
        author.id = id
        author.firstName = "First name"
        author.lastName = "Last name"
        repository.save(author)

        //when
        val authorEntity = repository.findByName(firstName = "x", lastName = "y")

        // then
        Assertions.assertThat(authorEntity).isNull()
    }


}