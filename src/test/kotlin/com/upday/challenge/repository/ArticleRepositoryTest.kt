package com.upday.challenge.repository

import com.upday.challenge.model.Article
import com.upday.challenge.model.Author
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.test.context.junit4.SpringRunner
import java.time.Instant
import java.util.*

@RunWith(SpringRunner::class)
@DataJpaTest
class ArticleRepositoryTest {
    @Mock
    lateinit var repository: ArticleRepository

    @Autowired
    lateinit var authorRepository: AuthorRepository

    @Test
    fun shouldSaveAndFetch() {
        // given
        val id = UUID.randomUUID()
        val article = Article()
        `when`(repository.findById(id)).thenReturn(Optional.of(article))

        article.id = id
        article.header = "Header"
        article.description = "Description"
        article.text = "Text"
        article.publishDate = Instant.now()
        article.authors = mutableSetOf(saveAuthor())
        article.keywords = listOf("key1","key2")
        repository.save(article)

        //when
        val articleEntity = repository.findByIdOrNull(id)

        // then
        Assertions.assertThat(articleEntity?.id).isNotNull()
        Assertions.assertThat(articleEntity?.header).isEqualTo("Header")
        Assertions.assertThat(articleEntity?.description).isEqualTo("Description")
        Assertions.assertThat(articleEntity?.text).isEqualTo("Text")
        Assertions.assertThat(articleEntity?.publishDate).isNotNull()
        Assertions.assertThat(articleEntity?.authors).isNotNull()
        Assertions.assertThat(articleEntity?.keywords).isNotNull()
    }


    private fun saveAuthor() : Author {
        val id = UUID.randomUUID()
        val author = Author()
        author.id = id
        author.firstName = "f"
        author.lastName = "l"
        return authorRepository.save(author)
    }

}