package com.upday.challenge.controller

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ArticleControllerTest() {

    @Test
    fun testCreateArticle() {
    }

    @Test
    fun testGetArticle() {
    }

    @Test
    fun testGetArticleByTime() {
    }

    @Test
    fun testGetArticleByAuthor() {
    }

    @Test
    fun testGetArticleByKeyword() {
    }


}