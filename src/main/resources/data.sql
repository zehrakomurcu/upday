DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS authors;

CREATE TABLE articles (
  id uuid NOT NULL PRIMARY KEY,
  header VARCHAR(250) NOT NULL,
  description VARCHAR(250) NOT NULL,
  text VARCHAR(250) DEFAULT NULL,
  publish_date timestamp with time zone
  );


CREATE TABLE authors (
  id uuid NOT NULL PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
 );

