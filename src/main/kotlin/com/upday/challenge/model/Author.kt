package com.upday.challenge.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonView
import com.upday.challenge.helper.Views
import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.validation.constraints.Size

@Entity
class Author {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    @JsonIgnore
    var id: UUID? = null

    @field: Size(max = 255)
    @JsonView(Views.Sanitised::class, Views.Public::class)
    lateinit var firstName: String

    @field: Size(max = 255)
    @JsonView(Views.Sanitised::class, Views.Public::class)
    lateinit var lastName: String

    @ManyToMany(mappedBy = "authors")
    @JsonIgnore
    var articles: MutableSet<Article>? = null
}