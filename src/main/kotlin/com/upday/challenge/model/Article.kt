package com.upday.challenge.model

import com.fasterxml.jackson.annotation.JsonView
import com.upday.challenge.helper.Views
import org.hibernate.annotations.GenericGenerator
import java.time.Instant
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Size

@Entity
class Article {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    @JsonView(Views.Sanitised::class, Views.Public::class)
    var id: UUID? = null

    @field: Size(max = 255)
    @JsonView(Views.Sanitised::class, Views.Public::class)
    var header: String? = null

    @JsonView(Views.Sanitised::class, Views.Public::class)
    var description: String? = null

    @JsonView(Views.Sanitised::class, Views.Public::class)
    var text: String? = null

    @JsonView(Views.Sanitised::class, Views.Public::class)
    var publishDate: Instant? = null

    @ManyToMany
    @JoinTable(
            name = "article_author",
            joinColumns = arrayOf(JoinColumn(name = "article_id", referencedColumnName = "id")),
            inverseJoinColumns = arrayOf(JoinColumn(name = "author_id", referencedColumnName = "id")))
    @JsonView(Views.Sanitised::class, Views.Public::class)
    lateinit var authors: MutableSet<Author>

    @ElementCollection
    @CollectionTable(name="keywords")
    var keywords: List<String>? = null
}


