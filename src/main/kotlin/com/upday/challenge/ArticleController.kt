package com.upday.challenge

import com.upday.challenge.dto.AuthorDto
import com.upday.challenge.dto.CreateArticleRequest
import com.upday.challenge.model.Article
import com.upday.challenge.model.Author
import com.upday.challenge.repository.ArticleRepository
import com.upday.challenge.repository.AuthorRepository
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import java.util.*

@RestController
class ArticleController(private val articleRepository: ArticleRepository,
                        private val authorRepository: AuthorRepository) {

    @PostMapping("/articles")
    @ResponseStatus(HttpStatus.CREATED)
    fun createArticle(@Validated @RequestBody request: CreateArticleRequest) : String {
        //Create author(s) if not exist
        var authorsSet = createAuthorsIfNeeded(request.authors)

        //Save article entity
        val article = articleRepository.save(
                Article().apply {
                    header = request.header
                    description = request.description
                    text = request.text
                    publishDate = request.publishDate?.toInstant()
                    authors = authorsSet
                    keywords = request.keywords
                })

        //Return created id of the entity
        return article.id.toString()
    }

    @GetMapping("/articles/{id}")
    fun findOne(@PathVariable id: String) : Optional<Article> =  articleRepository.findById(UUID.fromString(id))


    @GetMapping("/articles")
    fun findByDate(@RequestParam(name = "begin", required = true)
                   @DateTimeFormat(pattern = "yyyy-MM-dd") start: Date,
                   @RequestParam(name = "end", required = true)
                   @DateTimeFormat(pattern = "yyyy-MM-dd") end: Date) : Iterable<Article> {
        return articleRepository.findByDateRange(start.toInstant(), end.toInstant())
    }

    @GetMapping("/articles/author")
    fun findByAuthor(@RequestParam(name = "firstName", required = true) firstName: String,
                     @RequestParam(name = "lastName", required = true) lastName: String) : Iterable<Article> {
        val author = authorRepository.findByName(firstName, lastName) ?: Author()
        if(author.id == null)
            throw HttpClientErrorException(HttpStatus.NOT_FOUND, "Given author not found")

        return articleRepository.findByAuthors(mutableSetOf(author))
    }

    @GetMapping("/articles/keyword")
    fun findByKeyword(@RequestParam(name = "param", required = true) keyword: String) : Iterable<Article> {
        return articleRepository.findByKeyword(keyword)
    }

    @DeleteMapping("/articles/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteById(@PathVariable id: String)  {
        if(!articleRepository.existsById(UUID.fromString(id)))
            throw HttpClientErrorException(HttpStatus.NOT_FOUND, "Given id not found")

        articleRepository.deleteById(UUID.fromString(id))
    }

    private fun createAuthorsIfNeeded(requestedAuthors: List<AuthorDto>) : MutableSet<Author> {
        //Initialize a set for author(s) of an article
        var authors : MutableSet<Author> = mutableSetOf()

        requestedAuthors.forEach { a ->
            val authorEntity = authorRepository.findByName(a.firstName, a.lastName)

            if(authorEntity != null)
                authors.add(authorEntity)
            else {
                val author = Author().apply {
                    firstName = a.firstName
                    lastName = a.lastName
                }
                authorRepository.save(author)
                authors.add(author)
            }
        }

        return authors
    }

}
