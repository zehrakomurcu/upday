package com.upday.challenge.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.validation.constraints.Size

@JsonIgnoreProperties(ignoreUnknown = true)
class CreateArticleRequest(
        @field: Size(max = 255)
        var header: String,
        var description: String?,
        var text: String?,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var publishDate: Date?,
        var authors: List<AuthorDto>,
        var keywords: List<String>?
)

class AuthorDto(var firstName: String,
                var lastName: String)