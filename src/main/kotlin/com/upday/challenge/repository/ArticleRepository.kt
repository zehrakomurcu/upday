package com.upday.challenge.repository

import com.upday.challenge.model.Article
import com.upday.challenge.model.Author
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.time.Instant
import java.util.*

interface ArticleRepository : CrudRepository<Article, UUID> {

    fun findByAuthors(authors: MutableSet<Author>): Iterable<Article>

    @Query("SELECT a FROM Article a WHERE a.publishDate > ?1 and a.publishDate < ?2")
    fun findByDateRange(begin: Instant, end: Instant): Iterable<Article>

    @Query("SELECT a FROM Article a JOIN a.keywords k WHERE k = :keyword")
    fun findByKeyword(keyword: String): Iterable<Article>
}