package com.upday.challenge.repository

import com.upday.challenge.model.Author
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface AuthorRepository : CrudRepository<Author, UUID> {
    @Query("SELECT a FROM Author a WHERE a.firstName = ?1 and a.lastName = ?2")
    fun findByName(firstName: String, lastName: String): Author?
}