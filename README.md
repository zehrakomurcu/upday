# Project Description

This project is implemented for **upday for Samsung** as an assignment project. It's a Spring Web Application project which developed by using Kotlin language.

## Build & Run

Since the build tool for this project is Gradle, you can build and run by using following gradle commands:

```bash
gradle build
gradle run
```

## Spring Profiles

There are two profiles defined in application.yml

The active profile is test which uses using in-memory h2 database as default. 

If you would like to persist your data you can use _dev_ profile which uses PostgreSQL. 
There is docker-compose.yml file in root directory which helps you to run postgresql in your local with defined configuration.


Use following command to run PostgreSQL server on your with docker:
```
docker-compose up 
```

## Run Application as Docker Container

In the root directory you can find the Dockerfile to dockerize the application. 

Go to root project and run following commands:

```
docker build -t challenge . 
docker run --rm -p 8080:8080 challenge
```


## REST APIs

### Create Article

Creates article for giving information.


URL path: ``` POST articles/```


Request Model: _header_ and _authors_ are required parameters.
```json
{
	"header": "Power of Now",
	"description": "this is a short description",
	"text": "this is a test",
	"publishDate": "2018-04-19 06:11:80",
	"authors": [
		{"firstName": "zehra", "lastName":"komurcu"},
		{"firstName": "echkart", "lastName":"tolle"}
		],
	"keywords": ["psychology", "philosophy"]	
}
```

Response Model: API will return id of the created entity if successful.

```ae709714-1fad-411a-8d44-c8fe99e07e8b```


### Get Single Article

Get article for giving id.

URL path: ```GET articles/{id}```

Response Model:
```json
{
    "id": "ae709714-1fad-411a-8d44-c8fe99e07e8b",
    "header": "Power of Now",
    "description": "this is a short description",
    "text": "text",
    "publishDate": "2018-04-19T06:12:20Z",
    "authors": [
        {
            "firstName": "zehra",
            "lastName": "komurcu"
        },
        {
            "firstName": "echkart",
            "lastName": "tolle"
        }
    ],
    "keywords": [
        "psychology",
        "philosophy"
    ]
}
```


### Get Articles by time range

Get articles published at for giving time range .

URL path: ```GET /articles?begin=2018-04-01&end=2018-05-01```

Response Model:
```json
{
    "id": "ae709714-1fad-411a-8d44-c8fe99e07e8b",
    "header": "Power of Now",
    "description": "this is a short description",
    "text": "text",
    "publishDate": "2018-04-19T06:12:20Z",
    "authors": [
        {
            "firstName": "zehra",
            "lastName": "komurcu"
        },
        {
            "firstName": "echkart",
            "lastName": "tolle"
        }
    ],
    "keywords": [
        "psychology",
        "philosophy"
    ]
}
```

### Get Articles by Given Author

Get articles for giving author's first name and last name.

URL path: ```GET /articles/author?firstName=zehra&lastName=komurcu```

Response Model:
```json
{
    "id": "ae709714-1fad-411a-8d44-c8fe99e07e8b",
    "header": "Power of Now",
    "description": "this is a short description",
    "text": "text",
    "publishDate": "2018-04-19T06:12:20Z",
    "authors": [
        {
            "firstName": "zehra",
            "lastName": "komurcu"
        },
        {
            "firstName": "echkart",
            "lastName": "tolle"
        }
    ],
    "keywords": [
        "psychology",
        "philosophy"
    ]
}
```

### Get Articles by Given Keyword

Get articles for giving specific keyword.

URL path: ```GET /articles/keyword?param=philosophy```

Response Model:
```json
{
    "id": "ae709714-1fad-411a-8d44-c8fe99e07e8b",
    "header": "Power of Now",
    "description": "this is a short description",
    "text": "text",
    "publishDate": "2018-04-19T06:12:20Z",
    "authors": [
        {
            "firstName": "zehra",
            "lastName": "komurcu"
        },
        {
            "firstName": "echkart",
            "lastName": "tolle"
        }
    ],
    "keywords": [
        "psychology",
        "philosophy"
    ]
}
```

### Delete an Article

Deletes article for giving id.


URL path: ``` DELETE articles/{id}```

Response Model: API will return 200 OK if successful.


## Postman Collections

You can find the postman collection for each REST API call in project's root directory. 

The file name:
```
upday.postman_collection.json
```


## Possible Improvements
 
* For the future developments, _service_ layer can be added in order to make _controllers_ tiny.
* Unfortunately, there is small coverage for testing in this project. Testing should definitely be improved.

## Author

Zehra Nur Komurcu